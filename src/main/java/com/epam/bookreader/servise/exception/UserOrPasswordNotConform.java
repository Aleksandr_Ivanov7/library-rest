package com.epam.bookreader.servise.exception;

public class UserOrPasswordNotConform extends Exception {
    public UserOrPasswordNotConform() {
        super();
    }

    public UserOrPasswordNotConform(String message) {
        super(message);
    }

    public UserOrPasswordNotConform(String message, Throwable cause) {
        super(message, cause);
    }

    public UserOrPasswordNotConform(Throwable cause) {
        super(cause);
    }

    protected UserOrPasswordNotConform(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
