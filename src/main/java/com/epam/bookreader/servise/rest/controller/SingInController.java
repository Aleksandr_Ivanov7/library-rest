package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import com.epam.bookreader.servise.exception.UserNotFoundException;
import com.epam.bookreader.servise.exception.UserOrPasswordNotConform;
import com.epam.bookreader.servise.rest.session.SessionToken;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.UUID;

@Path("/sing_in")
public class SingInController {
    @GET
    public Response getUser(@QueryParam("login") String login, @QueryParam("password") String password) {
        try {
            User user = authenticate(login, password);
            String authToken = issueToken();
            SessionToken.add(user, authToken);
            return Response.ok("{ \"" + SessionToken.TOKEN_AUTH + "\" : \"" + authToken + "\"}").build();
        } catch (SQLException | UserNotFoundException | UserOrPasswordNotConform e) {
            return Response.status(Response.Status.FORBIDDEN).entity(e).build();
        }
    }

    private User authenticate(String login, String password) throws SQLException, UserNotFoundException, UserOrPasswordNotConform {
        User user = new UserDao().getByLogin(login);
        if (user == null) throw new UserNotFoundException("User not found.");
        if (!user.getPassword().equals(password)) throw new UserOrPasswordNotConform("Login or password not conform.");
        return user;
    }

    private String issueToken() {
        return UUID.randomUUID().toString();
    }
}
