package com.epam.bookreader.servise.rest.filter.authentication;

import com.epam.bookreader.servise.rest.session.SessionToken;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Authentication
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext){
        String authorizationToken = requestContext.getHeaderString(SessionToken.TOKEN_AUTH);
        try {
            checkToken(authorizationToken, requestContext);
        } catch (Exception e) {
            abortWithUnauthorized(requestContext);
        }
    }

    private void abortWithUnauthorized(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
    }

    private void checkToken(String authorizationToken, ContainerRequestContext requestContext){
        if (authorizationToken == null || !SessionToken.isValid(authorizationToken))
            abortWithUnauthorized(requestContext);
    }
}
