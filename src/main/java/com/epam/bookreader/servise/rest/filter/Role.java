package com.epam.bookreader.servise.rest.filter;

public enum Role {
    ADMIN,
    MODERATOR,
    USER
}
