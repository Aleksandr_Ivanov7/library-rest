package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.servise.rest.session.SessionToken;

import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/sing_out")
public class SingOutController {
    @DELETE
    public Response logout(@HeaderParam(SessionToken.TOKEN_AUTH) String token_auth){
        return Response.ok(SessionToken.delete(SessionToken.currentUser(token_auth))).build();
    }
}
