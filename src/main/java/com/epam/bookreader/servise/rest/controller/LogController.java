package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.LogDao;
import com.epam.bookreader.entity.Log;
import com.epam.bookreader.servise.rest.JSON;
import com.epam.bookreader.servise.rest.filter.Role;
import com.epam.bookreader.servise.rest.filter.access.AccessRole;
import com.epam.bookreader.servise.rest.filter.authentication.Authentication;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.SQLException;


@Authentication
@AccessRole({Role.ADMIN, Role.MODERATOR})
@Path("/logs")
public class LogController extends Controller<LogDao, Log> {
    @GET
    public Response getLogs() {
        return Response.status(Response.Status.OK).entity("Use path with login. For example: \"logs/{login}\"").build();
    }

    @GET
    @Path("/{login}")
    public Response getLogsByLogin(@PathParam("login") String login) {
        String json;
        try {
            json = new JSON<Log>().toJson(new LogDao().getAll(login));
        } catch (JsonProcessingException | SQLException e) {
            return Response.status(500).entity(e).build();
        }
        return Response.status(Response.Status.OK).entity(json).build();
    }

    @POST
    @Consumes("application/json")
    public Response add(Log log) {
        return super.add(log, new LogDao());
    }
}
