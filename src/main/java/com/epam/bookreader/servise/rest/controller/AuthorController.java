package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.servise.rest.filter.Role;
import com.epam.bookreader.servise.rest.filter.access.AccessRole;
import com.epam.bookreader.servise.rest.filter.authentication.Authentication;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Authentication
@AccessRole({Role.ADMIN, Role.USER})
@Path("/authors")
public class AuthorController extends Controller<AuthorDao, Author> {

    @GET
    public Response getAll() {
        return super.getAll(new AuthorDao());
    }

    @GET
    @Path("/{uuid}")
    public Response get(@PathParam("uuid") String uuid) {
        return super.get(uuid, new AuthorDao());
    }

    @POST
    @Consumes("application/json")
    public Response add(Author author) {
        return super.add(author, new AuthorDao());
    }

    @PUT
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response edit(@PathParam("uuid") String uuid, Author author) {
        return super.edit(uuid, author, new AuthorDao());
    }

    @DELETE
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response delete(@PathParam("uuid") String uuid) {
        return super.delete(new Author(uuid), new AuthorDao());
    }
}