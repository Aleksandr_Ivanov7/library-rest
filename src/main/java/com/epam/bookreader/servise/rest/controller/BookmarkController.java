package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.BookmarkDao;
import com.epam.bookreader.entity.Bookmark;
import com.epam.bookreader.servise.rest.filter.Role;
import com.epam.bookreader.servise.rest.filter.access.AccessRole;
import com.epam.bookreader.servise.rest.filter.authentication.Authentication;
import com.epam.bookreader.servise.rest.session.SessionToken;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Authentication
@AccessRole({Role.ADMIN, Role.USER})
@Path("/bookmarks")
public class BookmarkController extends Controller<BookmarkDao, Bookmark> {
    @GET
    public Response getAll() {
        return super.getAll(new BookmarkDao());
    }

    @GET
    @Path("/{uuid}")
    public Response get(@PathParam("uuid") String uuid) {
        return super.get(uuid, new BookmarkDao());
    }

    @POST
    @Consumes("application/json")
    public Response add(@HeaderParam(SessionToken.TOKEN_AUTH) String token_auth, Bookmark bookmark) {
        bookmark.setUser(SessionToken.currentUser(token_auth));
        return super.add(bookmark, new BookmarkDao());
    }

    @PUT
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response edit(@HeaderParam(SessionToken.TOKEN_AUTH) String token_auth, @PathParam("uuid") String uuid, Bookmark bookmark) {
        bookmark.setUser(SessionToken.currentUser(token_auth));
        return super.edit(uuid, bookmark, new BookmarkDao());
    }

    @DELETE
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response delete(@PathParam("uuid") String uuid) {
        return super.delete(new Bookmark(uuid), new BookmarkDao());
    }

}
