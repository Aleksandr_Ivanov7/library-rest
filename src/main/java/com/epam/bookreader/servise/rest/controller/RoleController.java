package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.entity.Role;
import com.epam.bookreader.servise.rest.filter.access.AccessRole;
import com.epam.bookreader.servise.rest.filter.authentication.Authentication;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Authentication
@AccessRole({com.epam.bookreader.servise.rest.filter.Role.ADMIN, com.epam.bookreader.servise.rest.filter.Role.MODERATOR})
@Path("/roles")
public class RoleController extends Controller<RoleDao, Role> {
    @GET
    public Response getAll() {
        return super.getAll(new RoleDao());
    }

    @GET
    @Path("/{uuid}")
    public Response get(@PathParam("uuid") String uuid) {
        return super.get(uuid, new RoleDao());
    }

    @POST
    @Consumes("application/json")
    public Response add(Role role) {
        return super.add(role, new RoleDao());
    }

    @PUT
    @Path("/{uuid}/edit")
    @Consumes("application/json")
    public Response edit(@PathParam("uuid") String uuid, Role role) {
        return super.edit(uuid, role, new RoleDao());
    }
}
