package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.Dao;
import com.epam.bookreader.entity.Entity;
import com.epam.bookreader.servise.rest.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ws.rs.core.Response;
import java.sql.SQLException;

abstract class Controller<T extends Dao, E extends Entity> {
    Response delete(E e, T t) {
        try {
            return Response.ok(t.delete(e)).build();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Response.status(500).entity("Error delete").build();
        }
    }

    Response edit(String uuid, E e, T t) {
        try {
            e.setUUID(uuid);
            t.update(e);
            return Response.status(Response.Status.OK).entity("Ok").build();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Response.status(500).entity("Error edit").build();
        }
    }

        Response add(E e, T t) {
        try {
            return Response.ok(t.save(e)).build();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return Response.status(500).entity("Error creating").build();
        }
    }

    Response get(String uuid, T t) {
        try {
            return Response.ok(new JSON().toJson((Entity) t.get(uuid))).build();
        } catch (SQLException | JsonProcessingException ex) {
            return Response.status(500).entity(ex).build();
        }
    }

    Response getAll(T t) {
        try {
            return Response.ok(new JSON<E>().toJson(t.getAll())).build();
        } catch (SQLException | JsonProcessingException ex) {
            return Response.status(500).entity(ex).build();
        }
    }
}
