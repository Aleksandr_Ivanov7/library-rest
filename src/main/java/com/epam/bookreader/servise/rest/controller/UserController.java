package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import com.epam.bookreader.servise.rest.filter.Role;
import com.epam.bookreader.servise.rest.filter.access.AccessRole;
import com.epam.bookreader.servise.rest.filter.authentication.Authentication;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Authentication
@AccessRole({Role.ADMIN, Role.MODERATOR})
@Path("/users")
public class UserController extends Controller<UserDao, User> {

    @GET
    public Response getAll() {
        return super.getAll(new UserDao());
    }

    @GET
    @Path("/{uuid}")
    public Response get(@PathParam("uuid") String uuid) {
        return super.get(uuid, new UserDao());
    }

    @POST
    @Consumes("application/json")
    public Response add(User user) {
        user.setUUID(user.generateUUID());
        return super.add(user, new UserDao());
    }

    @PUT
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response edit(@PathParam("uuid") String uuid, User user) {
        return super.edit(uuid, user, new UserDao());
    }

    @DELETE
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response delete(@PathParam("uuid") String uuid) {
        return super.delete(new User(uuid), new UserDao());
    }
}

