package com.epam.bookreader.servise.rest;

import com.epam.bookreader.entity.Entity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class JSON <T extends Entity>{
    public String toJson(Entity entity) throws JsonProcessingException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(entity);
    }
    public String toJson(List<T> entity) throws JsonProcessingException {
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(entity);
    }
}
