package com.epam.bookreader.servise.rest.filter.access;

import com.epam.bookreader.servise.exception.RoleException;
import com.epam.bookreader.servise.rest.filter.Role;
import com.epam.bookreader.servise.rest.session.SessionToken;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@AccessRole
@Provider
@Priority(Priorities.AUTHORIZATION)
public class AccessRoleFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Get the resource class which matches with the requested URL
        // Extract the roles declared by it
        Class<?> resourceClass = resourceInfo.getResourceClass();
        List<Role> classRoles = extractRoles(resourceClass);
        // Get the resource method which matches with the requested URL
        // Extract the roles declared by it
        Method resourceMethod = resourceInfo.getResourceMethod();
        List<Role> methodRoles = extractRoles(resourceMethod);

        try {
            // Check if the user is allowed to execute the method
            // The method annotations override the class annotations
            if(methodRoles.isEmpty()) checkPermissions(classRoles, requestContext);
            else checkPermissions(methodRoles, requestContext);
        } catch (Exception e) {
            requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
        }
    }

    // Extract the roles from the annotated element
    private List<Role> extractRoles(AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return new ArrayList<>();
        } else {
            AccessRole accessRole = annotatedElement.getAnnotation(AccessRole.class);
            if (accessRole == null) {
                return new ArrayList<>();
            } else {
                Role[] allowedRoles = accessRole.value();
                return Arrays.asList(allowedRoles);
            }
        }
    }

    private void checkPermissions(List<Role> allowedRoles, ContainerRequestContext requestContext) throws Exception {
        // Check if the user contains one of the allowed roles
        // Throw an Exception if the user has not permission to execute the method
        String currentRole = Objects.requireNonNull(
                SessionToken.currentUser(requestContext.getHeaderString(SessionToken.TOKEN_AUTH))).getRole().getName();
        if (!allowedRoles.toString().toLowerCase().contains(currentRole.toLowerCase())) throw new RoleException();
    }
}
