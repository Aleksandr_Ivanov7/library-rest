package com.epam.bookreader.servise.rest.controller;

import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.Book;
import com.epam.bookreader.servise.rest.filter.Role;
import com.epam.bookreader.servise.rest.filter.access.AccessRole;
import com.epam.bookreader.servise.rest.filter.authentication.Authentication;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Authentication
@AccessRole({Role.ADMIN, Role.USER})
@Path("/books")
public class BookController extends Controller<BookDao, Book> {

    @GET
    public Response getAll() {
        return super.getAll(new BookDao());
    }

    @GET
    @Path("/{uuid}")
    public Response get(@PathParam("uuid") String uuid) {
        return super.get(uuid, new BookDao());
    }

    @POST
    @Consumes("application/json")
    public Response add(Book book) {
        return super.add(book, new BookDao());
    }

    @PUT
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response edit(@PathParam("uuid") String uuid, Book book) {
        return super.edit(uuid, book, new BookDao());
    }

    @DELETE
    @Path("/{uuid}")
    @Consumes("application/json")
    public Response delete(@PathParam("uuid") String uuid) {
        return super.delete(new Book(uuid), new BookDao());
    }
}
